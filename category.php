

<?php
/**
 * The template for displaying Category Archive pages
 */
?>
<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>



<?php if ( have_posts() ): ?>

<section class="single-column-block <?php if($noPaddingBottom){ ?> no-padding-bottom <?php } ?>">
	<div class="container">
		<div class="row x-center">
			<h2><?php echo single_cat_title( '', false ); ?></h2>
			<p>Do you have a question for a UCLA Curtis Center mathematician or math educator? Send it our way and we will forward it to a specialist in that field. We can handle anything from topology to teaching strategies, from Common Core to combinatorics, and from differential equations to differentiated instruction. See answers to previous questions below.</p>
			<a class="btn" href="http://curtisucla.wpengine.com/ask-your-question/">Ask a Question</a>
		</div>
	</div>
</section>

<?php while ( have_posts() ) : the_post(); ?>

<section class="single-column-block <?php if($noPaddingBottom){ ?> no-padding-bottom <?php } ?>">
	<div class="container">
		<div class="row x-center">

			<div class="column col-8 blog">

				<h2><a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
				<?php the_excerpt(); ?>
				<a href="<?php esc_url( the_permalink() ); ?>" class="btn">See Answer</a>

			</div>
		</div>
	</div>
</section>

			

<?php endwhile; ?>

<?php else: ?>
<h2>No posts to display in <?php echo single_cat_title( '', false ); ?></h2>
<?php endif; ?>

<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>