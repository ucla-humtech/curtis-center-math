<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 */
?>
<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/flex-content' ) ); ?>

<?php Sightbox_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>