
<?php  
  // These are the arguments for your new wp query you are creating
  $loop_args = array (
    'post_type' => 'resources',
    'posts_per_page' => 3,
  );

  $resources_loop = new WP_Query( $loop_args );

?>



<section class="resources-block">
	<div class="resources">
		<div class="container">
			<h2 class="text-center">Latest Resources</h2>
			<div class="resource-list">
				<?php while ( $resources_loop->have_posts() ) : $resources_loop->the_post(); ?>
					<?php $thumbnail = get_the_post_thumbnail_url();  ?>
					<div class="resource-item">
						<div class="module">
							<h3 class="headline"><span style="background-color: #ffc72f ;"></span><?php the_title(); ?></h3>
							<!-- <a href="<?php //the_permalink(); ?>" class="resource-photo" style="background-image: url(<?php //echo $thumbnail; ?>);">
								
							</a> -->
							<div class="resource-copy">					
								<div><?php the_excerpt(); ?></div>
							</div>
							<a href="<?php the_permalink(); ?>">Read More <span>&rarr;</span></a>
						</div>
					</div>
				<?php endwhile ?>
				<?php wp_reset_query(); ?>
			</div>
			<div class="text-center"><a href="/resource-library/" class="btn">See All</a></div>
		</div>
	</div>
</section>