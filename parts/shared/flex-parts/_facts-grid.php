<section class="facts-grid <?php if( !empty(get_sub_field('logo_bg_color'))){ the_sub_field('logo_bg_color'); } ?>">
	<div class="container">
		<h2 class="text-center"><?php the_sub_field('section_title'); ?></h2>
		<ul class="facts-list">
			<?php while ( have_rows("facts") ) : the_row(); ?>
				<?php 
					$title = get_sub_field('title');
					$subtitle = get_sub_field('subtitle');
					$image = get_sub_field('image');
					$icons = get_sub_field('icons');
					$link = get_sub_field('link');
					$page_link = get_sub_field('page_link');
					$brief_desc = get_sub_field('brief_description');            
                    $fa_select_fac =  get_sub_field('font_awesome_type_facts');
				?>
				    <li <?php if( !empty(get_sub_field('card_bg_img')) || $link || $page_link ){ ?>class="<?php if( !empty(get_sub_field('card_bg_img'))){ echo 'card_bg_img'; } ?> <?php if ($link || $page_link){ echo 'hover';} ?>"<?php } ?> <?php if( !empty(get_sub_field('card_bg_img'))){ ?>style="background-image: url(<?php the_sub_field('card_bg_img'); ?>);"<?php } ?>>
				<?php if ($link || $page_link): ?>
					<a href="<?php if($link){ echo $link; } else { echo $page_link; }  ?>">
				<?php endif; ?>
                <?php if ($icons): ?>
                    <span class="icons"><i class="<?php echo $fa_select_fac; ?> fa-<?php echo $icons; ?>"></i></span>
				<?php endif; ?>                
                <?php if ($image): ?>     
				    <span><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></span>
				<?php endif; ?>
                <?php if(!empty($title)): ?>
				    <h4 class="toptxt_header <?php if( !empty(get_sub_field('size_of_title'))){ the_sub_field('size_of_title'); } ?>"><?php echo $title; ?></h4>
                <?php endif ?>
                <?php if(!empty($subtitle)): ?>
				    <h5 class="btmtxt_header <?php if( !empty(get_sub_field('sub_size_of_title'))){ the_sub_field('sub_size_of_title'); } ?>"><?php echo $subtitle; ?></h5>
                <?php endif ?>
                    <?php if ($brief_desc): ?>                        
                        <p>
                            <?php $content = $brief_desc;
                                //$limit = get_field('word_limit');
                                $limit = 15;
                                $trimmed_content = wp_trim_words( $content, $limit, '...' );
                                echo $trimmed_content; 
                            ?>
                        </p>
				    <?php endif; ?>
				<?php if ($link || $page_link): ?>
					</a>
				<?php endif; ?>
                        </li>
			<?php endwhile ?>
		</ul>
	</div>
</section>