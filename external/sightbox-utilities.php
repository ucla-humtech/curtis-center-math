<?php

	/**
	 * Sightbox_Utilities
	 */
	 
	 class Sightbox_Utilities {

        /**
         * Register nav menus
         **/

        public static function wp_menus() {
            register_nav_menus(
                array(
                'main-menu' => __('Main Nav'),
                'nav-cta' => __('Nav CTA'),
                'super-nav' => __('Super Nav'),
                'mobile-nav' => __('Mobile Nav'),
                )
            );
        }

    	/**
    	 * Print a pre formatted array to the browser - very useful for debugging
    	 **/
    	public static function print_a( $a ) {
    		print( '<pre>' );
    		print_r( $a );
    		print( '</pre>' );
    	}

    	/**
    	 * Simple wrapper for native get_template_part()
    	 * Allows you to pass in an array of parts and output them in your theme
    	 * e.g. <?php get_template_parts(array('part-1', 'part-2')); ?>
    	 **/
    	public static function get_template_parts( $parts = array() ) {
    		foreach( $parts as $part ) {
    			get_template_part( $part );
    		};
    	}

    	/**
    	 * Pass in a path and get back the page ID
    	 * e.g. Sightbox_Utilities::get_page_id_from_path('about/terms-and-conditions');
    	 **/
    	public static function get_page_id_from_path( $path ) {
    		$page = get_page_by_path( $path );
    		if( $page ) {
    			return $page->ID;
    		} else {
    			return null;
    		};
    	}

    	/**
    	 * Append page slugs to the body class
    	 * NB: Requires init via add_filter('body_class', 'add_slug_to_body_class');
    	 */
    	public static function add_slug_to_body_class( $classes ) {
    		global $post;
	   
    		if( is_page() ) {
    			$classes[] = sanitize_html_class( $post->post_name );
    		} elseif(is_singular()) {
    			$classes[] = sanitize_html_class( $post->post_name );
    		};

    		return $classes;
    	}
	
    	/**
    	 * Get the category id from a category name
    	 */
    	public static function get_category_id( $cat_name ){
    		$term = get_term_by( 'name', $cat_name, 'category' );
    		return $term->term_id;
    	}


        public static function disable_embed(){
            wp_dequeue_script( 'wp-embed' );
        }

        public static function disable_pingback( &$links ) {
            foreach ( $links as $l => $link )
            if ( 0 === strpos( $link, get_option( 'home' ) ) )
            unset($links[$l]);
        }

        public static function stop_heartbeat() {
            wp_deregister_script('heartbeat');
        }

        public static function wpdocs_dequeue_dashicon() {
                if (current_user_can( 'update_core' )) {
                    return;
                }
                wp_deregister_style('dashicons');
        }
	
    }
